import sqlite from "sqlite";
import SQL from "sql-template-strings";

const initializeDatabase = async () => {
  const db = await sqlite.open("./db.sqlite");

  /**
   * retrieves the cat-Collection from the database
   */
  const getCatCollection = async () => {
    let returnString = "";
    const rows = await db.all(
      "SELECT cat_id AS id, age, color, sex FROM catcollection"
    );
    rows.forEach(
      ({ id, age, color, sex }) =>
        (returnString += `[id:${id}] - ${age} - ${color} - ${sex}`)
    );
    return returnString;
  };

  const controller = {
    getCatCollection
  };

  return controller;
};

export default initializeDatabase;
