import app from "./app";
import initializeDatabase from "./db";

const start = async () => {
  const controller = await initializeDatabase();
  app.get("/", (req, res) => res.send("ok"));

  app.get("/cat/collection", async (req, res) => {
    const cat_collection = await controller.getCatCollection();
    res.send(cat_collection);
  });

  app.listen(8080, () => console.log("server listening on port 8080"));
};
start();
